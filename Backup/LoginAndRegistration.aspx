﻿<%@ Page Language="VB"  %>
<%@ Import Namespace="System.Data.OleDb" %>

<script  runat="server">
    Sub submit(ByVal sender As Object, ByVal e As EventArgs)
        Dim dbconn, sql, dbcomm, dbread
       
        
        dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\MyUsers.mdb"))
        dbconn.Open()
        sql = "SELECT * FROM Users WHERE Username ='" & txt1.Text & "' AND Password = '" & tb2.Text & "'"
        dbcomm = New OleDbCommand(sql, dbconn)
        dbread = dbcomm.ExecuteReader()
        Users.DataSource = dbread
        Users.DataBind()
        dbread.Close()
        dbconn.Close()
        lbl1.Text = "Hello " & txt1.Text & "!"
        If (Users.Items.Count <> 0) Then
            Session("Fname") = txt1.Text
            Response.Redirect("DetailsViewControl.aspx")
        Else
            lbl1.Text = "You are not registered!"
        End If
    End Sub
    Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim dbconn, sql, dbcomm, dbread
        Dim NoOfRecords As Integer
        dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\MyUsers.mdb"))
        dbconn.Open()
        sql = "SELECT * FROM Users WHERE Username ='" & txt1.Text & "' AND Password = '" & tb2.Text & "'"
        dbcomm = New OleDbCommand(sql, dbconn)
        dbread = dbcomm.ExecuteReader()
        Users.DataSource = dbread
        Users.DataBind()
        dbread.Close()
        lbl1.Text=Users.Items.Count
        If (Users.Items.Count = 0) Then
            dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\MyUsers.mdb"))
            dbconn.Open()
            sql = "SELECT * FROM Users"
            dbcomm = New OleDbCommand(sql, dbconn)
            dbread = dbcomm.ExecuteReader()
            Users.DataSource = dbread
            Users.DataBind()
            dbread.Close()
            dbconn.Close()
            NoOfRecords = Users.Items.Count
            dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\MyUsers.mdb"))
            dbconn.Open()
            sql = "INSERT INTO Users VALUES(" & NoOfRecords + 1  & " , '" & txt1.Text & "' , '" & tb2.Text & "')"
            dbcomm = New OleDbCommand(sql, dbconn)
            dbread = dbcomm.ExecuteReader()
            Users.DataSource = dbread
            Users.DataBind()
            
            dbread.Close()
            dbconn.Close()
            lbl1.Text = "Hello " & txt1.Text & "! You are now registered!"
            
        Else
            lbl1.Text = "You have already registered! Please log in!"
        End If
       
    End Sub
    
    
    
</script>

<html>
<body>
<script Language="JavaScript">
<!--
    function Length_TextField_Validator() {
        // Check the length of the value of the element named text_name
        // from the form named form_name if it's < 3 and > 10 characters
        // display a message asking for different input
        if ((Form2.txt1.value.length < 3) || (Form2.txt1.value.length > 10)) {
            // Build alert box message showing how many characters entered
            mesg = "You have entered " + Form2.txt1.value.length + " character(s)\n"
            mesg = mesg + "Valid entries are between 3 and 10 characters.\n"
            mesg = mesg + "Please verify your input and submit again."
            alert(mesg);
            // Place the cursor on the field for revision
            Form2.txt1.focus();
            // return false to stop further processing
            return (false);
        }
        // If text_name is not null continue processing
        return (true);
    }
-->
</script>

<form id="Form2" runat="server"
onsubmit = "return Length_TextField_Validator()">
Username: <asp:TextBox id="txt1" runat="server" />
<br /><br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
ControlToValidate="txt1"
Text="The name field is required!"
runat="server" />
</br>

Password:
<asp:TextBox id="tb2" TextMode="password" runat="server" />
<br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
ControlToValidate="tb2"
Text="The password field is required!"
runat="server" />
<br /> 


<asp:Button ID="Button1" OnClick="submit" Text="Login" runat="server" 
    Width="50px"  />
<asp:Button ID="Button2" runat="server" Text="Register" Width="62px" 
    onclick="Button2_Click" />
<p>
    <asp:Label ID="lbl1" runat="server"></asp:Label>
 
</p>
<asp:Repeater id="Users" runat="server" Visible="False">

<HeaderTemplate>
<table border="1" width="100%">
<tr>
<th>ID</th>
<th>Username</th>
<th>Password</th>
</tr>
</HeaderTemplate>

<ItemTemplate>
<tr>
<td><%#Container.DataItem("ID")%></td>
<td><%#Container.DataItem("Username")%></td>
<td><%#Container.DataItem("Password")%></td>
</tr>
</ItemTemplate>

<FooterTemplate>
</table>
</FooterTemplate>

</asp:Repeater>
</form>
</body>
</html>
