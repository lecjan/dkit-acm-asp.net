﻿
<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReadingFromXMLFile.aspx.vb" Inherits="Project_Skeleton___Web_Forms.ReadingFromXMLFile" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <form id="form2" action="ReadingFromXMLFile.aspx">
    <div>    
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/App_Data/XMLFile1.xml">    
    </asp:XmlDataSource>    
    </div>
     <asp:GridView runat="server" ID="Gridview1" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="XmlDataSource1">
         <Columns>
         
             <asp:BoundField DataField="comment" HeaderText="Testimonials" SortExpression="comments" />
         </Columns>
    </asp:GridView>
    </form>

</asp:Content>
