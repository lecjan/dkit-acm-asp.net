﻿<%@ Page Title="Intro Page" Language="vb" AutoEventWireup="false"
    CodeBehind="Default.aspx.vb" Inherits="Project_Skeleton___Web_Forms._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form runat="server">
    <div class="page" style="background:none">

        <div class="header" style="background:none">
        </div>

        <div class="main" style="background:none">
        </div>

        <div class="clear" style="background:none">
        </div>

        <div class="footer">   
            <asp:label id="Message" font-size="9" font-bold="false" runat="server"/>
            <br />
            <asp:LinkButton ID="LinkButtonEnter" runat="server" BackColor="#660033" BorderStyle="Dotted" BorderWidth="1px" PostBackUrl="Home.aspx">E N T E R</asp:LinkButton>
        </div>
    </div>
    </form>
</body>
</html>