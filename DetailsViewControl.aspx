﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DetailsViewControl.aspx.vb" Inherits="Project_Skeleton___Web_Forms.DetailsViewControl" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<div id="list">
<form id="Form2" action="DetailsViewControl.aspx">

    <asp:SqlDataSource ID="acm_members" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionStringACM %>" ProviderName="<%$ ConnectionStrings:ConnectionStringACM.ProviderName %>" SelectCommand="SELECT [ACMID], [Firstname], [Lastname], [Post] FROM [Members] ORDER BY [Lastname], [Firstname]"></asp:SqlDataSource>

    <asp:ListView ID="ListView1" runat="server" DataKeyNames="ACMID" DataSourceID="acm_members">
        <AlternatingItemTemplate>
            <tr style="">
                <td>
                    <asp:Label ID="ACMIDLabel" runat="server" Text='<%# Eval("ACMID") %>' />
                </td>
                <td>
                    <asp:Label ID="FirstnameLabel" runat="server" Text='<%# Eval("Firstname") %>' />
                </td>
                <td>
                    <asp:Label ID="LastnameLabel" runat="server" Text='<%# Eval("Lastname") %>' />
                </td>
                <td>
                    <asp:Label ID="PostLabel" runat="server" Text='<%# Eval("Post") %>' />
                </td>
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                </td>
                <td>
                    <asp:Label ID="ACMIDLabel1" runat="server" Text='<%# Eval("ACMID") %>' />
                </td>
                <td>
                    <asp:TextBox ID="FirstnameTextBox" runat="server" Text='<%# Bind("Firstname") %>' />
                </td>
                <td>
                    <asp:TextBox ID="LastnameTextBox" runat="server" Text='<%# Bind("Lastname") %>' />
                </td>
                <td>
                    <asp:TextBox ID="PostTextBox" runat="server" Text='<%# Bind("Post") %>' />
                </td>
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table runat="server" style="">
                <tr>
                    <td>No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                </td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="FirstnameTextBox" runat="server" Text='<%# Bind("Firstname") %>' />
                </td>
                <td>
                    <asp:TextBox ID="LastnameTextBox" runat="server" Text='<%# Bind("Lastname") %>' />
                </td>
                <td>
                    <asp:TextBox ID="PostTextBox" runat="server" Text='<%# Bind("Post") %>' />
                </td>
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="">
                <td>
                    <asp:Label ID="ACMIDLabel" runat="server" Text='<%# Eval("ACMID") %>' />
                </td>
                <td>
                    <asp:Label ID="FirstnameLabel" runat="server" Text='<%# Eval("Firstname") %>' />
                </td>
                <td>
                    <asp:Label ID="LastnameLabel" runat="server" Text='<%# Eval("Lastname") %>' />
                </td>
                <td>
                    <asp:Label ID="PostLabel" runat="server" Text='<%# Eval("Post") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table id="itemPlaceholderContainer" runat="server" border="0" style="">
                            <tr runat="server" style="">
                                <th runat="server">ACMID</th>
                                <th runat="server">Firstname</th>
                                <th runat="server">Lastname</th>
                                <th runat="server">Post</th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" style="">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="False" ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="">
                <td>
                    <asp:Label ID="ACMIDLabel" runat="server" Text='<%# Eval("ACMID") %>' />
                </td>
                <td>
                    <asp:Label ID="FirstnameLabel" runat="server" Text='<%# Eval("Firstname") %>' />
                </td>
                <td>
                    <asp:Label ID="LastnameLabel" runat="server" Text='<%# Eval("Lastname") %>' />
                </td>
                <td>
                    <asp:Label ID="PostLabel" runat="server" Text='<%# Eval("Post") %>' />
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>

</form>
</div>

</asp:Content>
