﻿<%@ Page Language="VB" MasterPageFile="~/Site.Master" EnableEventValidation="false" %>

<%@ Import Namespace="System.Data" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.OleDb" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

<script  runat="server">

    Sub Page_Load()
        Dim Username As String = Session("Uname")
        Dim UserLoggedIn As Boolean = Session("UserLoggedIn")
        Session("UserLoggedIn") = False
    End Sub
    
    Sub submit(ByVal sender As Object, ByVal e As EventArgs)
        Dim dbconn, sql, dbcomm, dbread, fullname
        Dim admintype As Boolean
        admintype = False
        Dim ds As New DataSet
        Dim aCookie As New HttpCookie("AccAdminType")
        
        dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\ACMDatabase.mdb"))
        dbconn.Open()
        sql = "SELECT * FROM Members WHERE Username ='" & txt1.Text & "' AND Password = '" & tb2.Text & "'"
        dbcomm = New OleDbCommand(sql, dbconn)
        dbread = dbcomm.ExecuteReader()
        Members.DataSource = dbread
        Members.DataBind()
        Try
            dbcomm.Fill(ds, "UserRecord")
        Catch ex As MissingMemberException
            ErrorMessage.Text = ex.Message.ToString
        End Try
            dbread.Close()
            dbconn.Close()
        
            If (Members.Items.Count <> 0) Then
                Session("Uname") = txt1.Text
                Session("Password") = tb2.Text
                Session("UserLoggedIn") = True
            Try
                fullname = ds.Tables("UserRecord").Rows(0).Item(6) + " " + ds.Tables("UserRecord").Rows(0).Item(6)
                admintype = ds.Tables("UserRecord").Rows(0).Item(4)
            Catch ex As NullReferenceException
                ErrorMessage.Text = ex.Message.ToString
            End Try
            Session("Ufullname") = fullname
            lbl1.Text = "Hello " & fullname & "!"
            
            
            
            'Response.Redirect("DetailsViewControl.aspx")
        Else
            lbl1.Text = "You are not a registered member!"
        End If
        
        
       
            aCookie.Value = admintype
            Response.Cookies.Add(aCookie)
        
    End Sub
    
    Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim dbconn, sql, dbcomm, dbread, dbwrite
        Dim NoOfRecords As Integer
        dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\ACMDatabase.mdb"))
        dbconn.Open()
        
        sql = "SELECT * FROM Members WHERE Username ='" & txt1.Text & "' AND Password = '" & tb2.Text & "'"
        
        dbcomm = New OleDbCommand(sql, dbconn)
        dbread = dbcomm.ExecuteReader()
        Members.DataSource = dbread
        Members.DataBind()
        dbread.Close()
        lbl1.Text = Members.Items.Count
        If (Members.Items.Count = 0) Then
            dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\ACMDatabase.mdb"))
            dbconn.Open()
            
            sql = "SELECT * FROM Members"
            
            dbcomm = New OleDbCommand(sql, dbconn)
            dbread = dbcomm.ExecuteReader()
            Members.DataSource = dbread
            Members.DataBind()
            dbread.Close()
            dbconn.Close()
            NoOfRecords = Members.Items.Count
            
            dbconn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("App_Data\ACMDatabase.mdb"))
            dbconn.Open()
            
            sql = "INSERT INTO Members ([ACMID],[Username],[Password]) VALUES('" & NoOfRecords + 1 & "','" & txt1.Text & "','" & tb2.Text & "');"
            
            dbcomm = New OleDbCommand(sql, dbconn)
            Try
                dbwrite = dbcomm.ExecuteNonQuery()
                Members.DataSource = dbwrite
                Members.DataBind()
                dbwrite.Close()
                dbconn.Close()
                lbl1.Text = "Hello " & txt1.Text & "! You are now registered!"
            Catch ex As OleDbException
                ErrorMessage.Text = ex.ToString
            End Try
            
            
        Else
            lbl1.Text = "You have already registered! Please log in!"
        End If
    End Sub
    
</script>


<script Language="JavaScript">
<!--
    function Length_TextField_Validator() {
        // Check the length of the value of the element named text_name
        // from the form named form_name if it's < 8 and > 20 characters
        // display a message asking for different input
        if ((Form2.txt1.value.length < 4) || (Form2.txt1.value.length > 20)) {
            // Build alert box message showing how many characters entered
            mesg = "You have entered " + Form2.txt1.value.length + " character(s)\n"
            mesg = mesg + "Valid entries are between 4 and 20 characters only.\n"
            mesg = mesg + "Please verify your input and submit again."
            alert(mesg);
            // Place the cursor on the field for revision
            Form2.txt1.focus();
            // return false to stop further processing
            return (false);
        }
        // If text_name is not null continue processing
        return (true);
    }

</script>


<form id="Form2" action="MembersLoginAndRegistration.aspx" onsubmit = "return Length_TextField_Validator()">
    Username: 
    
    <asp:TextBox id="txt1" runat="server" />
    <br /><br />
    
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
        ControlToValidate="txt1"
        Text="The name field is required!"
        runat="server" />
    </br>
    Password:
    
    <asp:TextBox id="tb2" TextMode="password" runat="server" />
    <br />
    
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
        ControlToValidate="tb2"
        Text="The password field is required!"
        runat="server" />
    <br /> 

    <asp:Button ID="Button1" OnClick="submit" Text="Login" runat="server" 
        Width="50px"  />

    <asp:Button ID="Button2" runat="server" Text="Register" Width="62px" 
        onclick="Button2_Click" />
    <br />
    <br />
    <br />
    <p>
    <asp:Label ID="lbl1" runat="server"></asp:Label>
    </p>
 
    <asp:Repeater id="Members" runat="server" Visible="False">

        <HeaderTemplate>
        <table border="1" width="100%">
        <tr>
        <th>ID</th>
        <th>Username</th>
        <th>Password</th>
        </tr>
        </HeaderTemplate>

        <ItemTemplate>
        <tr>
        <td><%#Container.DataItem("ACMID")%></td>
        <td><%#Container.DataItem("Username")%></td>
        <td><%#Container.DataItem("Password")%></td>
        </tr>
        </ItemTemplate>

        <FooterTemplate>
        </table>
        </FooterTemplate>

    </asp:Repeater>
</form>

    <asp:Label ID="ErrorMessage" runat="server" Text=""></asp:Label>

</asp:Content>

