﻿<%@ Page Title="Home Page" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeBehind="Home.aspx.vb" Inherits="Project_Skeleton___Web_Forms._Home" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to 
        Dundalk Institute of Technology 
        ACM Student Chapter!
    </h2>
    <br>
    <p>
    Student Chapter of Association for Computing Machinery established in April 2014

    within Dundalk Institute of Technology. Our goal is to excel student's ideas 

    in self- and projects development leading us to the best professional practice 

    and to encourage creative thinking in our research for beneficial solution for humanity.
    </p>
</asp:Content>
