﻿<%@ Page Language="vb" AutoEventWireup="false"MasterPageFile="~/Site.Master" CodeBehind="ContactUs.aspx.vb" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  
    <title></title>
</head>
<body>

    <div>
       
        <br />
        Postal Address: 

        Dundalk Institute of Technology, Dublin Road, Dundalk, County Louth, Ireland
    </div>

     <div>
       <br />
         
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d37541.28715099118!2d-6.415117208247176!3d53.97915669603559!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4860cc1442ef182d%3A0x95dcf5b33c4e4546!2sDundalk+Institute+of+Technology!5e0!3m2!1sen!2sie!4v1417560612040" frameborder="0" style="border-style: none; border-color: inherit; border-width: thin; height: 233px; width: 436px;" id="I1" name="I1"></iframe>
         
    </div>
     

    <form id="form1">

    <fieldset>
         			<legend>
         				Enter your Contact Information and send Inquiry
         			</legend>
         			<table>
            			<tr>
               				<td class="labelcell">
               					Name<span style="color: red">*</span>
               				</td>
               				<td class="inputcell2">
                  				<input class="text" name="name" id="name" size="25" />
               				</td>
            			</tr>
            			<tr>
               				<td class="labelcell">
               					Email:<span style="color: red">*</span>
               				</td>

               				<td class="inputcell2">
                  				<input class="text" name="email" id="email" size="25" />
               				</td>
            			</tr>
                         <tr>
               				<td class="labelcell">
               					Subject:<span style="color: red">*</span>
               				</td>

               				<td class="inputcell2">
                  				<input class="text" name="subject" id="subject" size="25" />
               				</td>
            			</tr>
            			<tr>
               				<td class="labelcell">
               					Message<span style="color: red">*</span>
               				</td>

               				<td class="inputcell2">
                  				<textarea id="message" name="message" cols="20" rows="8"></textarea>
               				</td>
            			</tr>
            			<tr>
               				<td colspan="2">
               					<span style="color: red">*</span> Required Field
               				</td>
            			</tr>
                         <tr>
               				<td colspan="2">

        			<input type="submit"  onclick="ContactUs.js()"value="Submit" />                 
     			</p>
               					
               				</td>
            			</tr>
                         	
         			</table>
      			</fieldset>
  </form>

   






   
</body>
</html>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="HeadContent">
    <style type="text/css">
        #send {
            width: 82px;
        }
    </style>
</asp:Content>

